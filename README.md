The code
========
The code is split into:

- `matcher.py`: Parses a single-line Request into it's components
- `log_analyzer.py`: Analyzes an entire log file of single-line requests

Run from Command Line
====================
To run the code from command line please use:
```bash
python log_analyzer.py -topcount <number> -file <file name> -bytes <number>
```

Tests
====
There are currently unit tests for `matcher.py` in `tester.py`

Comments
=========
The output format of **Requests per Day** and **Top hosts making requests** is not ideal for printing but is ideal for downstream processing (chose to keep them as dicts)