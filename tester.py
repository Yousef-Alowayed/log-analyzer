#------------------ Imports
import unittest
from matcher import parse_request


class TestMatcher(unittest.TestCase):
    """
    Test regex and group matching
    """

    """Parse a simple request for testing"""
    simple_host         = 'host.com'
    simple_date         = '01/Aug/1995'
    simple_time         = '00:00:07'
    simple_time_code    = '0400'
    simple_type         = 'GET'
    simple_url          = '/root/index.html'
    simple_request_code = '304'
    simple_bytes        = '10'

    simple_request = simple_host + ' - - [' +\
                     simple_date + ':' + \
                     simple_time + ' -' +\
                     simple_time_code + '] "' +\
                     simple_type + ' ' +\
                     simple_url + '" ' + \
                     simple_request_code + ' ' +\
                     simple_bytes

    parsed_request = parse_request(simple_request)

    def test_parse_host_simple(self):
        self.assertEqual(TestMatcher.parsed_request['host'], TestMatcher.simple_host)

    def test_parse_date_simple(self):
        self.assertEqual(TestMatcher.parsed_request['date'], TestMatcher.simple_date)

    def test_parse_time_simple(self):
        self.assertEqual(TestMatcher.parsed_request['time'], TestMatcher.simple_time)

    def test_parse_time_code_simple(self):
        self.assertEqual(TestMatcher.parsed_request['time_code'], TestMatcher.simple_time_code)

    def test_parse_type_simple(self):
        self.assertEqual(TestMatcher.parsed_request['type'], TestMatcher.simple_type)

    def test_parse_url_simple(self):
        self.assertEqual(TestMatcher.parsed_request['url'], TestMatcher.simple_url)

    def test_parse_request_code_simple(self):
        self.assertEqual(TestMatcher.parsed_request['request_code'], TestMatcher.simple_request_code)

    def test_parse_bytes_simple(self):
        self.assertEqual(TestMatcher.parsed_request['bytes'], TestMatcher.simple_bytes)

if __name__ == '__main__':
    unittest.main()
