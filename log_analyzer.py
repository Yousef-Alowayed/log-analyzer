#------------------ Imports
from matcher import parse_request
import sys
import argparse


def analyze_file(file_name='NASA_access_log_Aug95', topcount = 10, max_bytes=10):
    """

    :param file_name:
    :return:
    """

    total_num_of_requests = 0
    num_of_requests_per_day = {}
    hosts_to_num_requests = {}
    request_url_with_5xx_to_bytes = {}      # {url, bytes}
    num_request_starting_with_finance = 0
    num_of_requests_with_large_bytes = 0    # bytes > bytes_max

    with open(file_name) as file_object:

        print '----- Opened file: ' + str(file_name)
        print '----- Reading lines of Request and Processing'

        for line in file_object:

            # print 'parsing line number: ' + str(total_num_of_requests)
            # print 'parsing line: ' + str(line)

            parsed_request  =   parse_request(line)

            #------- Save parsed data into variables

            host            =   parsed_request['host']
            date            =   parsed_request['date']
            time            =   parsed_request['time']
            time_code       =   parsed_request['time_code']

            # If the request is broken (GET and url are not standard characters)
            # set type to '' and save GET+url into url
            if 'type' in parsed_request:
                type        =   parsed_request['type']
            else:
                type = ''

            url             =   parsed_request['url']
            request_code    = parsed_request['request_code']
            bytes           = parsed_request['bytes']
            if bytes == '-':
                bytes = 0
            bytes = int(bytes)


            #------- Analysis

            # 1 Total number of requests
            total_num_of_requests += 1

            # 2 Total number of requests per day
            if date in num_of_requests_per_day:
                num_of_requests_per_day[date] += 1
            else:
                num_of_requests_per_day[date] = 1

            # 3 The hosts making requests and their count
            if host in hosts_to_num_requests:
                hosts_to_num_requests[host] += 1
            else:
                hosts_to_num_requests[host] = 1

            # 4 url with 5xx code to number of bytes (if bytes > 0)
            if (request_code.startswith('5') and (bytes > 0)):
                if url in request_url_with_5xx_to_bytes:
                    request_url_with_5xx_to_bytes[url] += bytes
                else:
                    request_url_with_5xx_to_bytes[url] = bytes

            # 5
            if url.startswith('/finance/'):
                num_request_starting_with_finance += 1

            # 6 Request with bytes > max_bytes
            if int(bytes) > max_bytes:
                num_of_requests_with_large_bytes += 1

    #------- Post Processing

    print '----- Post Processing Data'

    # 3. Top -topcount hosts making requests and their count
    top_hosts_to_num_requests = dict(
        sorted(hosts_to_num_requests.iteritems(), key = lambda item: item[1], reverse=True)[:topcount]
    )

    # 4. List of 5xx Request urls in descending byte order
    five_hundred_requests_descending = sorted(
        request_url_with_5xx_to_bytes.iteritems(), key=lambda item: item[1], reverse=True
    )

    analysis_dict = {
        '1. Total Number of Requests'   : total_num_of_requests,
        '2. Requests per Day'          : num_of_requests_per_day,
        '3. Top '+str(topcount)+' number of hosts making requests and their count' : top_hosts_to_num_requests,
        '4. 5xx Request urls in descending byte order' : five_hundred_requests_descending,
        '5. Total Requests Starting with /finance/' : num_request_starting_with_finance,
        '6. Requests with Bytes > '+str(max_bytes) : num_of_requests_with_large_bytes
    }

    return analysis_dict


def test():
    file_name = 'test_log'

    analysis = analyze_file(topcount=2)

    sorted_analysis = sorted(
        analysis.iteritems(), key=lambda item: int(item[0][0]), reverse=False
    )

    print '----- Results: \n'

    for (description, results) in sorted_analysis:
        print description + '\n'
        print results
        print '---------------------------------'

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("-topcount", help="")
    parser.add_argument("-file", help="")
    parser.add_argument("-bytes", help="")

    args = parser.parse_args()

    file_name   = args.file
    topcount    = int(args.topcount)
    max_bytes   = int(args.bytes)

    print 'topcount = ' + str(topcount)

    analysis = analyze_file(file_name=file_name, topcount=topcount, max_bytes=max_bytes)

    sorted_analysis = sorted(
        analysis.iteritems(), key=lambda item: int(item[0][0]), reverse=False
    )

    print '----- Results: \n'

    for (description, results) in sorted_analysis:
        print description + '\n'
        print results
        print '---------------------------------'

if __name__ == '__main__':
    main()
