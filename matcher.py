#------------------ Imports
import re

#------------------ Static Methods
def parse_request(request):
    """
    Parse a single line request string and return a dictionary mapping the name of each component of the request
        to it's component

    :param (str) request: a single line request of the form
                        (host name) - - [dd/Mon/yyyy:hh:mm:ss -0400] "GET (url)" (3 digit request code) (num bytes)
    :return (dict): dictionary mapping request components to their strings
                        {
                            'host'          :   'example.com'                   ,
                            'date'          :   '01/Aug/1995'                   ,
                            'time'          :   '00:00:08'                      ,
                            'time_code'     :   '0400'                          ,
                            'type'          :   'GET'                           ,
                            'url'           :   '/history/skylab/skylab.html'   ,
                            'request_code'  :   '200'                           ,
                            'bytes'         :   '155'
                        }
    """

    # Patterns
    host_pattern            = '([a-zA-Z0-9_\.]*)'           # group 1
    date_pattern            = '(\d\d/\w\w\w/\d\d\d\d)'      # group 2
    time_pattern            = '(\d\d:\d\d:\d\d)'            # group 3
    time_code_pattern       = '(\d\d\d\d)'                  # group 4
    request_type_pattern    = '(\w*)'                       # group 5
    request_url_pattern     = '(.*)'                        # group 6
    request_code_pattern    = '(\d\d\d)'                    # group 7
    request_bytes_pattern   = '([\d-]*)'                    # group 8

    # Full request pattern
    request_pattern     =   host_pattern            + \
                            '\s-\s-\s\['            + \
                            date_pattern            + \
                            ':'                     + \
                            time_pattern            + \
                            '\s-'                   + \
                            time_code_pattern       + \
                            '\]\s\"'                + \
                            request_type_pattern    + \
                            '\s'                    + \
                            request_url_pattern     + \
                            '\"\s'                  + \
                            request_code_pattern    + \
                            '\s'                    + \
                            request_bytes_pattern

    # Broken request pattern (GET and url are not standard characters)
    broken_request_pattern =host_pattern + \
                            '\s-\s-\s\[' + \
                            date_pattern + \
                            ':' + \
                            time_pattern + \
                            '\s-' + \
                            time_code_pattern + \
                            '\]\s\"' + \
                            request_url_pattern + \
                            '\"\s' + \
                            request_code_pattern + \
                            '\s' + \
                            request_bytes_pattern

    match = re.search(request_pattern, request)



    if not match:
        match = re.search(broken_request_pattern, request)

        parsed_dict = {
            'host': match.group(1),
            'date': match.group(2),
            'time': match.group(3),
            'time_code': match.group(4),
            'url': match.group(5),
            'request_code': match.group(6),
            'bytes': match.group(7)
        }

        return parsed_dict

    if not match:
        raise RuntimeError('no matches found for request: ' + str(request));

    parsed_dict = {
        'host'          : match.group(1),
        'date'          : match.group(2),
        'time'          : match.group(3),
        'time_code'     : match.group(4),
        'type'          : match.group(5),
        'url'           : match.group(6),
        'request_code'  : match.group(7),
        'bytes'         : match.group(8)
    }

    return parsed_dict

def main():
    pass

if __name__ == '__main__':
    main()

